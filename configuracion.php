<?php
@session_start();

/**
 * Constante con la carpeta base del proyecto
 */
define("_BASE_PATH", str_replace("\\", "", $_SESSION["_BASE_PATH"])); // == "\/"?"":$_SESSION["_BASE_PATH"]
/**
 * Constante con la URL base del proyecto incluyendo dominio
 */
define("_BASE_URL", "http://" . $_SERVER["SERVER_NAME"] . _BASE_PATH);
/**
 * Constante con la hubicacion del proyecto
 */
define("_BASE_DIR", $_SERVER["DOCUMENT_ROOT"] . "" . _BASE_PATH);


function __autoload($classname) {
    $partes = explode("\\", $classname);
    $include_path = _BASE_DIR . '';
    $archivo = implode("/", $partes) . ".php";
    $filename = $include_path . $archivo;
    include_once($filename);
}
