<?php

namespace Libs\Generador;

class c_Cabecero extends a_DevuelveHTML {
    /* Propiedades */

    private $datos;

    /* Métodos */

    //Método constructor
    public function __construct(array $datos) {
        $this->datos = $datos;
        $this->construirHTML();
    }

    //Método  que construye el html
    protected function construirHTML() {
        //Componentes
        $base_url = _BASE_URL;
        //Menu Principal
        $barra_principal = new c_BarraPrincipal();
        //Llenar la variable html
        $this->html = <<<HTML
<!DOCTYPE html>
<html>
	<head>
		<title>{$this->datos['titulo']}</title>
		<!-- etiquetas meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">

		<!-- favicon -->
		<link rel="icon" type="image/png" href="{$base_url}recursos/img/favicon.png">
		<!-- Hojas de estilo -->
{$this->incluirCSS()}
		<!-- script -->
{$this->incluirJS()}
	</head>
	<body>
		<span id="perfil_user" style="display:none"></span>
		<a class="oculto" id="st_enlace_ciego" href="http://www.google.com" target="_blank"><i class="fa fa-download">&nbsp; </i>&nbsp;</a>
		<header>
{$barra_principal->obtenerHTML()}
		</header>\n
HTML;
    }

    private function incluirCSS() {
        $contenido = '';
        $base_url = _BASE_URL;
        if (isset($this->datos['css'])) {
            if (gettype($this->datos['css']) == 'string') {
                $contenido .= <<<CSS
		<link rel="stylesheet" type="text/css" href="{$base_url}{$this->datos['css']}">\n
CSS;
            } else if (gettype($this->datos['css']) == 'array') {
                foreach ($this->datos['css'] as $css) {
                    $contenido .= <<<CSS
		<link rel="stylesheet" type="text/css" href="{$base_url}{$css}">\n
CSS;
                }
            }
        }
        return $contenido;
    }

    private function incluirJS() {
        $base_url = _BASE_URL;
        $contenido = '';
        if (isset($this->datos['js'])) {
            if (gettype($this->datos['js']) == 'string') {
                $contenido .= <<<JS
		<script src="{$base_url}{$this->datos['js']}"></script>\n
JS;
            } else if (gettype($this->datos['js']) == 'array') {
                foreach ($this->datos['js'] as $script) {
                    $contenido .= <<<JS
		<script src="{$base_url}{$script}"></script>\n
JS;
                }
            }
        }
        return $contenido;
    }

}
