<?php

namespace Libs\Generador;

abstract class a_DevuelveHTML{
	/* propiedades */
	protected $html;

	/* Metodos */
	
	//implementar método para construir html
	abstract protected function construirHTML();

	//implementar método para devolver html
	public function obtenerHTML(){
		return $this->html;
	}
}