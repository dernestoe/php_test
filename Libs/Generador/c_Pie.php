<?php

namespace Libs\Generador;

class c_Pie extends a_DevuelveHTML {
    /* Propiedades */

    private $datos;

    /* Métodos */

    //Método constructor
    public function __construct(array $datos) {
        $this->datos = $datos;
        $this->construirHTML();
    }

    //Método  que construye el html
    protected function construirHTML() {
        $base_dir = _BASE_URL;
        $year = date('Y');
        $this->html = <<<HTML
        <footer>
            <p class="p1">Copyright &copy;{$year} . Todos los derechos resevados</p>
            <p class="p2">Con la programación de <a href="http://neverbit.com">NeverBit</a></p>			
        </footer>
        {$this->incluirJS()}
    </body>
</html>
HTML;
    }

    private function incluirJS() {
        $base_dir = _BASE_URL;
        $contenido = '';
        if (isset($this->datos['js'])) {
            if (gettype($this->datos['js']) == 'string') {
                $contenido .= <<<JS
		<script src="{$base_dir}{$this->datos['js']}"></script>\n
JS;
            } else if (gettype($this->datos['js']) == 'array') {
                foreach ($this->datos['js'] as $script) {
                    $contenido .= <<<JS
		<script src="{$base_dir}{$script}"></script>\n
JS;
                }
            }
        }
        return $contenido;
    }

}
