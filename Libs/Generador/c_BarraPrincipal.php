<?php

namespace Libs\Generador;

class c_BarraPrincipal extends a_DevuelveHTML
{
	/* Propiedades */

	/* Métodos */

	//Constructor
	public function __construct(){
		$this->construirHTML();
	}

	//Construir html
	protected function construirHTML(){
		$nombre = @$_SESSION["nickname"];
		$img  = @$_SESSION["img"];
		$fecha = date("Y-m-d",strtotime(date("Y-m-d") . ' + 1 day'));
                $base_url = _BASE_URL;
		$this->html = <<<HTML
			<section id="barra_principal">
				<div class="logo">
					<a href="{$base_url}index" data-tooltip="Ir a la página inicial">
						<img src="{$base_url}/Vista/img/logo.png" alt="logotipo">
					</a>
				</div>
				<div class="alternar_menu">
					<div class="cont">
						<i class="fa fa-reorder bg_negro pointer c_blanco t18"></i>
					</div>
				</div>
				<div class="alternar_barra">
					<div class="cont">
						<i class="fa fa-chevron-down bg_negro pointer c_blanco t18"></i>
					</div>
				</div>
			<div id="loading_general"> Cargando ... </div>
HTML;
	}
}
