<?php

namespace Libs\Controller;

class Controller {

    public function retorno($c, $a) {
        eval('$controlador = new \\Controlador\\' . $c . '();');
        if (is_object($controlador)) {
            if (method_exists($controlador, $a)) {
                $archivo = $this->tipoRetorno($controlador->$a());
            } else {
                $archivo = "El controlador $c no tiene un methodo $a";
            }
        } else {
            $archivo = "El controlador $c no existe";
        }
        return $archivo;
    }
    
    private function tipoRetorno(a_ReturnController $retorno, $c, $a){
        if ($retorno instanceof a_ReturnHTML or
                $retorno instanceof a_ReturnJson){
            return $retorno->getRetorno();
            
        }
        return false;
    }

}
