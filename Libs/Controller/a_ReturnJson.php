<?php

namespace Libs\Controller;

class a_ReturnJson implements a_ReturnController{
    private $variables;
    public function __construct(array $variables) {
        $this->variables = $variables;
    }
    public function getRetorno(){
        return json_encode($this->variables);
    }
}
