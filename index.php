<?php

session_start();
$_SESSION["_BASE_PATH"] = dirname($_SERVER["SCRIPT_NAME"]) . "/";
require_once('configuracion.php');
if (empty($_GET["url"])) {
    header("location: " . _BASE_URL . "index");
} else {
    $partes = explode("/", $_GET["url"]);
    $c = ucfirst($partes[0]);
    if (empty($partes[1])) {
        $a = "index";
    } else {
        $a = ucfirst($partes[1]);
    }
    eval('$controlador = new Controlador\\' . $c . '();');
    if (is_object($controlador)) {
        if (method_exists($controlador, $a)) {
            $archivo = $controlador->$a();
        } else {
            $archivo = "El controlador $c no tiene un methodo $a";
        }
    } else {
        $archivo = "El controlador $c no existe";
    }

    /*
      ###################################################
      ################### Cabecero ######################
      ###################################################
     */
    //Crear el array de opciones para el cabecero
    $h_datos = array(
        'titulo' => ucfirst($partes[0]),
        'css' => array(
            'Vista/css/compradores.css'
            ,'Vista/css/jquery-ui.css'
            , 'Vista/css/' . $partes[0] . '.css'
        ),
        'js' => array(
            "Vista/js/modernizr.js",
            "Vista/js/jquery.js",
            "Vista/js/jquery-ui.js"
        )
    );
    //crear una instancia del objeto Cabecero
    $cabecero = new \Libs\Generador\c_Cabecero($h_datos);

    //Crear el array de opciones para el pie
    $f_datos = array(
        'js' => array(            
            'server/js/' . $partes[0] . '.js'
        )
    );
    //crear una instancia del objeto pie
    $pie = new \Libs\Generador\c_Pie($f_datos);

    /*
     * Impresion de la pagina con todos los elementos
     */

    //Imprimir en pantalla lo que devuelve el cabecero
    echo $cabecero->obtenerHTML();
    //Imprimir el contenido de la pagina
    echo $archivo;
    //Imprimir en pantalla lo que devuelve el pie
    echo $pie->obtenerHTML();
}

